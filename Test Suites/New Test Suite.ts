<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>88f57b10-338a-4344-bee5-e7eb4d7e31f8</testSuiteGuid>
   <testCaseLink>
      <guid>5543ae47-ecac-43af-84c4-f0b6d313d893</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCase2</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c0a0387d-f91c-46c5-8285-845d63134285</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/New Test Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>c0a0387d-f91c-46c5-8285-845d63134285</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>UserName</value>
         <variableId>8e96194c-2731-4c41-87df-e3aad6d8e6de</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c0a0387d-f91c-46c5-8285-845d63134285</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>51f25e88-1cf0-4fea-966d-5af4e5edc9d7</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
